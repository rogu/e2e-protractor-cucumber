Feature: Log in

    Background: prepare home page
        Given I open home page and log out

    Scenario Outline: user login
        When I input "<username>" and "<password>" and then click login button
        Then I should see "<info>"
        Examples:
            | username        | password    | info                     |
            | admin@localhost | admin       | you are logged in        |
            | badusername     | 123456      | user not found           |
            | admin@localhost | badpassword | bad username or password |
