Feature: Items View
  I would like to add and search items

  Background:
    Given I am on the items view and log in

  Scenario Outline: Add new item
    When I fill form with <title> <price> <category> and click add-item button
    And I type <title> into the search field
    Then I should see added item
    Examples:
      | title  | price | category |
      | "Bob"  | "101" | "food"   |
      | "Mike" | "102" | "food"   |
      | "Kate" | "103" | "food"   |

  Scenario Outline: Form Errors
    When I fill form again with <title> <price> <category>
    Then I should see <errors>
    Examples:
      | title    | price | category | errors |
      | product1 | 10    | null     | 1      |
      | product2 | null  | null     | 2      |
      | null     | null  | null     | 3      |
