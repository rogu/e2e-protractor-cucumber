import { Given, Then, When } from 'cucumber';
import { browser } from 'protractor';
import { MainPage } from './login.po';
import { expect } from 'chai';

let page: MainPage = new MainPage();

Given('I open home page and log out', async () => {
    await browser.get('/home');
    await page.logOut();
});

When('I input {string} and {string} and then click login button',
    async (username, password) => await page.logIn(username, password));

Then('I should see {string}', async (info) =>
    expect(await page.locators.info.getText()).to.be.equal(info)
);
