import { browser, ExpectedConditions } from 'protractor';
import { BasePage, LocatorTypes } from '../../support/page.po';

export class MainPage extends BasePage {

    authComponent = this.ElementLocator(LocatorTypes.TagName, 'app-auth');

    locators = {
        items: this.ElementLocator(LocatorTypes.Css, '[routerlink=items]'),
        workers: this.ElementLocator(LocatorTypes.Css, '[routerlink=workers]'),
        register: this.ElementLocator(LocatorTypes.Css, '[routerlink=register]'),
        profile: this.ElementLocator(LocatorTypes.Css, '[routerlink=profile]'),
        usernameField: this.ElementLocator(LocatorTypes.Name, 'username', this.authComponent),
        passwordField: this.ElementLocator(LocatorTypes.Name, 'password', this.authComponent),
        btnLogIn: this.ElementLocator(LocatorTypes.ButtonText, 'log in', this.authComponent),
        btnLogOut: this.ElementLocator(LocatorTypes.ButtonText, 'log out', this.authComponent),
        info: this.ElementLocator(LocatorTypes.ClassName, 'badge-info', this.authComponent)
    }

    async logIn(username = 'admin@localhost', password = 'admin') {
        try {
            return await this.waitForLogin;
        } catch (error) {
            this.locators.usernameField.clear();
            this.locators.passwordField.clear();
            this.locators.usernameField.sendKeys(username);
            this.locators.passwordField.sendKeys(password);
            this.locators.btnLogIn.click();
            return await this.locators.info.getText();
        }
    }

    async logOut() {
        try {
            await this.locators.btnLogOut.isDisplayed();
            this.locators.btnLogOut.click();
        } catch (error) {
            return true
        }
    }

    get waitForLogin(): PromiseLike<any> {
        return browser.wait(ExpectedConditions.presenceOf(this.locators.btnLogOut), 2500);
    }
}
