import { browser } from 'protractor';
import { BasePage, LocatorTypes } from '../../support/page.po';

export class ItemsPage extends BasePage {

    form = this.ElementLocator(LocatorTypes.Id, 'add-item-form');
    uniqueTitle = 'Robert' + Date.now();
    locators = {
        btnAddItem: this.ElementLocator(LocatorTypes.ButtonText, 'add item'),
        form: this.form,
        category: this.ElementLocator(LocatorTypes.TagName, 'select', this.form),
        title: this.ElementLocator(LocatorTypes.Css, '[placeholder=title]', this.form),
        price: this.ElementLocator(LocatorTypes.Css, '[placeholder=price]', this.form),
        btnSend: this.ElementLocator(LocatorTypes.ButtonText, 'send item'),
        dataGridRows: this.ElementLocator(LocatorTypes.Css, 'app-datagrid tbody tr', null, true),
        search: this.ElementLocator(LocatorTypes.Id, 'search-by-title'),
        errors: this.ElementLocator(LocatorTypes.Xpath, '//small[contains(text(),\'pole wymagane\')]', this.form, true)
    }

    public navigateTo() {
        return browser.get('items');
    }

    async addItem(title, price, category) {
        await this.locators.btnAddItem.click();
        await this.locators.form.isPresent();
        await this.locators.category.sendKeys(category);
        await this.locators.title.sendKeys(title);
        await this.locators.price.sendKeys(price);
        return this.locators.btnSend.click();
    }

}
