import { Given, Then, When } from 'cucumber';
import { expect } from 'chai';
import { ItemsPage } from './items.po';
import { MainPage } from '../login/login.po';
import { convertNull } from '../../support/utils';

const mainPage: MainPage = new MainPage();
const page: ItemsPage = new ItemsPage();
const now: number = Date.now();

/* Add Items */

Given('I am on the items view and log in', async () => {
    await page.navigateTo();
    await mainPage.logIn();
});

When('I fill form with {string} {string} {string} and click add-item button',
    async (title, price, category) => await page.addItem(title + now, price, category));

When('I type {string} into the search field',
    async (title) => await page.locators.search.sendKeys(title + now));

Then('I should see added item',
    async () => expect(await page.locators.dataGridRows.count()).to.be.eql(1));

/* Display Errors */

When(/^I fill form again with (.*) (.*) (.*)$/, async (title, price, category) => {
    await page.addItem(convertNull(title) ? convertNull(title) + Date.now() : '', convertNull(price), convertNull(category));
});

Then('I should see {int}', async function (expectedErrorCount) {
    const formErrorCount = await page.locators.errors.count();
    expect(formErrorCount).to.be.equal(expectedErrorCount)
});
