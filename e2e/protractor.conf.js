// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './features/**/*.feature'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'https://demo-ng-app.debugger.pl/',
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    // require step definition files before executing features
    require: ['./steps/**/*.ts', './support/*.ts'],
    // <boolean> fail if there are any undefined or pending steps
    strict: true,
    // <boolean> invoke formatters without executing steps
    dryRun: false,
    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    compiler: [],
    tags: '~@ignore'
  },
  suites: {
    items: './features/items/items.feature',
    math: './features/math/math.feature'
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    //browser.driver.manage().window().setPosition(250, 0);
  }
};
