import { readFile, utils, WorkSheet } from 'xlsx';
import { Status } from "cucumber";
import { browser } from 'protractor';

const path = require('path');
const fs = require('fs');

export const Colors = {
    red: "\x1b[31m",
    green: "\x1b[32m",
    yellow: "\x1b[33m",
    blue: "\x1b[34m",
    magenta: "\x1b[35m",
    cyan: "\x1b[36m"
}

export const convertNull = (value) => (value === 'null') ? '' : value;

export class Utils {

    static ReadExcelFile(file: string): WorkSheet {
        const url = path.join(__dirname, 'data', file);
        const workbook = readFile(url);
        const sheet = workbook.Sheets['Arkusz1'];
        return utils.sheet_to_json(sheet);
    }

    static async ScreenshotWhenFail(scenario) {
        if (scenario.result.status === Status.FAILED) {
            const screenShot = await browser.takeScreenshot();
            const time = new Date().toLocaleTimeString("en-US", { hour12: false }).replace(/:/g, _ => '-');
            const link = path.resolve(__dirname, 'screenshots', scenario.pickle.name + '_' + time + '.png');
            fs.writeFile(link, screenShot, 'base64', function (err) {
                if (err) throw err;
                console.log('Fail saved in screenshots folder.');
            });
        }
    }
}
