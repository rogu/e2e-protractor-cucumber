import { element, By, ElementFinder } from 'protractor';

export enum LocatorTypes {
    Css,
    Id,
    Xpath,
    Name,
    ChildText,
    ClassName,
    TagName,
    ButtonText
}

export class BasePage {

    locators: { [key: string]: ElementFinder };

    ElementLocator(type: LocatorTypes, spec: any, parent?: ElementFinder, all?: boolean): ElementFinder {

        const el = parent ? parent[all ? 'all' : 'element'].bind(parent) : all ? element.all : element;

        switch (type) {
            case LocatorTypes.Css:
                return el(By.css(spec));
            case LocatorTypes.Id:
                return el(By.id(spec));
            case LocatorTypes.Xpath:
                return el(By.xpath(spec));
            case LocatorTypes.Name:
                return el(By.name(spec));
            case LocatorTypes.ChildText:
                return el(By.cssContainingText(spec.tag, spec.value));
            case LocatorTypes.ClassName:
                return el(By.className(spec));
            case LocatorTypes.TagName:
                return el(By.tagName(spec));
            case LocatorTypes.ButtonText:
                return el(By.buttonText(spec));
            default:
                return el(By.css(spec));
        }
    }

    getParent(el: ElementFinder) {
        return this.ElementLocator(LocatorTypes.Xpath, '../..', el);
    }

}
