import { BeforeAll, Before, After, setDefaultTimeout } from 'cucumber';
import { Utils, Colors } from './utils';
import { browser } from 'protractor';

setDefaultTimeout(15000);

BeforeAll(async () => {
    console.log('before all');
});

Before(async (scenario) => {
    console.log(Colors.yellow, 'before', scenario.pickle.name);
})

After(async (scenario) => {
    Utils.ScreenshotWhenFail(scenario);
    await browser.sleep(2000);
});
